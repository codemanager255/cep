//
//  LoginScreenViewController.swift
//  CEP
//
//  Created by Brett on 1/15/20.
//  Copyright © 2020 Mobile Apps Company. All rights reserved.
//

import UIKit

class LoginScreenViewController: UIViewController {
    
    @IBOutlet var loginView: UIView!  {
           didSet {
               let layer = loginView.layer
               layer.cornerRadius = 10.0
               layer.masksToBounds = true
            let myColor : UIColor = UIColor( red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0 )

            layer.borderColor = myColor.cgColor
               layer.borderWidth = 2.0
           }
       }
    @IBOutlet var signInButton: UIButton! {
              didSet {
                  let layer = signInButton.layer
                layer.cornerRadius = 10.0
                  layer.masksToBounds = true
                let myColor = signInButton.backgroundColor
                layer.borderColor = myColor?.cgColor
                  layer.borderWidth = 2.0
              }
          }
    
    @IBOutlet var userField: UITextField!
    
    @IBOutlet var passField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func showPassAction(_ sender: Any) {
    }
    
    @IBAction func signInAction(_ sender: Any) {
    }
    
    @IBAction func signInHelp(_ sender: Any) {
        let storyboard = UIStoryboard(name: "SignInHelp", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "NavController") as! UINavigationController
        myVC.modalPresentationStyle = .fullScreen
        self.present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func continueAsGuest(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "FirstScreen") as! UITabBarController
        myVC.modalPresentationStyle = .fullScreen
        self.present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func createAccount(_ sender: Any) {
    }
    
    @IBAction func showPasswordAction(_ sender: Any) {
        if (passField.isSecureTextEntry == true) {
            passField.isSecureTextEntry = false
        }
        else{
            passField.isSecureTextEntry = true
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  EntertainmentScreenViewController.swift
//  CEP
//
//  Created by Brett on 1/29/20.
//  Copyright © 2020 Mobile Apps Company. All rights reserved.
//

import UIKit

class EntertainmentScreenViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func previewAction(_ sender: Any) {
    }
    
    @IBAction func learnMoreAction(_ sender: Any) {
    }
    
    @IBAction func personalDeviceEntertainmentLink(_ sender: Any) {
    }
    
    @IBAction func backAction(_ sender: Any) {
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

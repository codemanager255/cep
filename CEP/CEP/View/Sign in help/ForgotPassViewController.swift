//
//  ForgotPassViewController.swift
//  CEP
//
//  Created by Brett on 1/23/20.
//  Copyright © 2020 Mobile Apps Company. All rights reserved.
//

import UIKit

class ForgotPassViewController: UIViewController {
    
    @IBOutlet var titleField: UIButton!
    
    @IBOutlet var firstNameField: UITextField!
    
    @IBOutlet var middleNameField: UITextField!
    
    @IBOutlet var lastNameField: UITextField!
    
    @IBOutlet var suffixField: UITextField!
    
    @IBOutlet var mileagePlusField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
   @IBAction func titleAction(_ sender: Any) {
    let storyboard = UIStoryboard(name: "SignInHelp", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "TitleView") as! TitleViewController
    myVC.passVC = self
    self.navigationController!.pushViewController(myVC, animated: true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func exitAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitAction(_ sender: Any) {
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

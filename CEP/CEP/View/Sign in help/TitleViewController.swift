//
//  TitleViewController.swift
//  CEP
//
//  Created by Brett on 1/23/20.
//  Copyright © 2020 Mobile Apps Company. All rights reserved.
//

import UIKit

class TitleViewController: UIViewController {
   
    @IBOutlet var titleTable: UITableView!{
        didSet{
            titleTable.dataSource = self
            titleTable.delegate = self
            titleTable.register(UITableViewCell.self, forCellReuseIdentifier: "cell")

        }
    }
    
    let options = ["Mr.",
                   "Mrs.",
                   "Ms.",
                   "Mx.",
                   "Dr.",
                   "Mstr.",
                   "Miss",
                   "Prof.",
                   "Rev.",
                   "Sir",
                   "Sister"]
    
    var passVC: ForgotPassViewController?
    var numVC: ForgotNumViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
            // Do any additional setup after loading the view.
        }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func exitAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func sendBack(_ title: String){
        if (passVC != nil){
            passVC?.titleField.setTitle(title, for: .normal)
            passVC?.titleField.setTitleColor( .black, for: .normal)
            self.navigationController?.popViewController(animated: true)
        }
        else {
            numVC?.titleField.setTitle(title, for: .normal)
            numVC?.titleField.setTitleColor( .black, for: .normal)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func clear(){
        passVC = nil
        numVC = nil
    }
}

        
extension TitleViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return options.count
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = options[indexPath.row]
        return cell
    }
}

extension TitleViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            sendBack("   Mr.")
        case 1:
            sendBack("   Mrs.")
        case 2:
            sendBack("   Ms.")
        case 3:
            sendBack("   Mx.")
        case 4:
            sendBack("   Dr.")
        case 5:
            sendBack("   Mstr.")
        case 6:
            sendBack("   Miss")
        case 7:
            sendBack("   Prof.")
        case 8:
            sendBack("   Rev.")
        case 9:
            sendBack("   Sir")
        case 10:
            sendBack("   Sister")
        default:
            break
        }
    }
}



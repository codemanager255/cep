//
//  AirportMapsVC.swift
//  CEP
//
//  Created by Consultant on 1/31/20.
//  Copyright © 2020 Mobile Apps Company. All rights reserved.
//

import UIKit

class AirportMapsVC: UIViewController {
    
    @IBOutlet weak var tableview: UITableView!
    var airportArray = [["Atlanta, GA(ATL)", "Austin, TX (AUS)", "Boston, MA (BOS)", "Chicago IL (ORD)", "Dallas/Fort Worth, TX (DFW)", "Denver, CO(DEN)", "Fort Lauderdale, FL (FLL)", "Frankfurt, DE (FRA)", "Honolulu, HI (HNL)", "Houston, TX (IAH)","Las Vegas, NV(LAS)", "London, GB (LHR)", "Lon Angeles, CA(LAX)", "Mexico City, MX (MEX)","Minneapolis/St. Paul, MN (MSP)","Munich, DE (MUC)", "New York, NY (LGA)", "New York/Newark, NJ (EWR)", "Orange County, CA (SNA)", "Orlando, FL (MCO)", "Philadelphia, PA (PHL)", "Phoenix, AZ (PHX)", "Portland, OR (PDX)", "San Diego, CA(SAN)", "San Francisco, CA (SFO)", "Seattle, WA (SEA)", "Tokyo, JP(NRT)", "Washington, DC (DCA)","Washington, DC (IAD)"]]
    var headerTitle = ["Explore our new airport maps."]

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

    @IBAction func cancelAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension AirportMapsVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return airportArray[section].count
    
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AirportMapsTableCell") as! AirportMapsTableCell
        
         let celltext = airportArray[indexPath.section][indexPath.row]
        
        cell.airportLabel.text = celltext
        cell.arrowImage.isHidden = true
        return cell

    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section < headerTitle.count {
            return headerTitle[section]
        }

        return nil
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
         if let headerView = view as? UITableViewHeaderFooterView {
            headerView.textLabel?.textColor = #colorLiteral(red: 0.003804810345, green: 0.2049998045, blue: 0.6020452976, alpha: 1)
           // headerView.textLabel?.font.xHeight = 12.0
           }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
    
}

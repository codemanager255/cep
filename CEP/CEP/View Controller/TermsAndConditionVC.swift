//
//  TermsAndConditionVC.swift
//  CEP
//
//  Created by Consultant on 1/23/20.
//  Copyright © 2020 Mobile Apps Company. All rights reserved.
//

import UIKit

class TermsAndConditionVC: UIViewController {

    @IBOutlet weak var t_cLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let attrTet = NSMutableAttributedString(string: "- This pass grants one person one-time access to a United club location subject to United Club terms &condition, uniter.com/UnitedClubAccess, each of which is subject to change, with or without notice. \n  - Access is subject to space availability.\n  - Same-day boarding pass is required for entry. \n Effective November 1, 2019, United Club customers,including members and their guests, and one-time pass holders are need to provide a same-day boarding pass for travel on United, Star Alliance or a contracted partner operated flight for entry into all United Club locations. \n  - Children under two years of age may accompany one-time pass users. \n - This pass may not be sold, bartered, traded, exchange or purchased.", attributes: nil)
        t_cLabel.numberOfLines = 0
        t_cLabel.attributedText = attrTet
        //testButton.setTitle(attrTet.string, forState: .Normal)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    

}

//
//  FAQViewController.swift
//  CEP
//
//  Created by Consultant on 1/25/20.
//  Copyright © 2020 Mobile Apps Company. All rights reserved.
//

import UIKit

class FAQViewController: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    
    let qArr = ["what are the benefits of a United club one-time pass?", "Can I bring my spouse, my child or a friend into United Club with me?", "When will my United Club one-time pass expire?", "Can a one-time be used at a star Alliance lounge?", "What if I purchase a United Club membership? May I apply the value of the one-time pass to the membership price?"]
    let aArr = ["A United Club one-time pass allows access to personalized travel assistance while visiting UnitedClub. Complimentary WiFi access and beverages are also available in United Club locations.","A United Club one-time pass grants one person , one-time access to any United Club location worldwide.Additional Companions may purchase their own one time passes before accompanying you into the United Club.","All one-time passes are valid for one year from the original purchase date.","United Club one-time passes are valid at any United Club location but are not valid at other Star Alliance lounges.", "Join within 30 days of your one-time pass purchase, and we will apply the value (maximum $50) to your United Club initiation fee."]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableview.delegate = self as! UITableViewDelegate
        tableview.dataSource = self as! UITableViewDataSource
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }

    
    @IBAction func cancelAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension FAQViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FAQTableViewCell") as! FAQTableViewCell
        cell.answerLabel.text = aArr[indexPath.row]
        cell.questionLabel.text = qArr[indexPath.row]
        return cell
        
    }
    
    
}

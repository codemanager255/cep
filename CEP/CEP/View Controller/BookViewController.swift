//
//  BookViewController.swift
//  CEP
//
//  Created by Sky on 1/20/20.
//  Copyright © 2020 Mobile Apps Company. All rights reserved.
//
import UIKit

class BookViewController: UIViewController {
    
    @IBOutlet weak var oneWayView: UIView!
    @IBOutlet weak var roundTripView: UIView!
    @IBOutlet weak var multipleView: UIView!
    @IBOutlet weak var recentView: UIView!
    @IBOutlet weak var segment: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        segment.selectedSegmentIndex = 0
        oneWayView.isHidden = false
        roundTripView.isHidden = true
        multipleView.isHidden = true
        recentView.isHidden = true
    }
    

    @IBAction func switchView(_ sender: Any) {
        if segment.selectedSegmentIndex == 0 {
            oneWayView.isHidden = false
            roundTripView.isHidden = true
            multipleView.isHidden = true
            recentView.isHidden = true
        }
        
        else if segment.selectedSegmentIndex == 1 {
            oneWayView.isHidden = true
            roundTripView.isHidden = false
            multipleView.isHidden = true
            recentView.isHidden = true
        }
        
        else if segment.selectedSegmentIndex == 2 {
            oneWayView.isHidden = true
            roundTripView.isHidden = true
            multipleView.isHidden = false
            recentView.isHidden = true
            }
            
        else if segment.selectedSegmentIndex == 3 {
            oneWayView.isHidden = true
            roundTripView.isHidden = true
            multipleView.isHidden = true
            recentView.isHidden = false
            }
    }


}

//
//  BuyClubPassVC.swift
//  CEP
//
//  Created by Consultant on 1/24/20.
//  Copyright © 2020 Mobile Apps Company. All rights reserved.
//

import UIKit

class BuyClubPassVC: UIViewController {

    @IBOutlet weak var continueToPurchaseButton: UIButton! {
        didSet{
            let layer = continueToPurchaseButton.layer
            layer.cornerRadius = 5.0
            layer.masksToBounds = true
            
            // Shadow and Radius
            continueToPurchaseButton.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
            continueToPurchaseButton.layer.shadowOffset = CGSize(width: 0.0, height: 8.0)
            continueToPurchaseButton.layer.shadowOpacity = 1.0
            continueToPurchaseButton.layer.shadowRadius = 10.0
            continueToPurchaseButton.layer.masksToBounds = false
            continueToPurchaseButton.layer.cornerRadius = 4.0
            
           
        }
    }
    @IBOutlet weak var priceDetailsLabel: UILabel!
    @IBOutlet weak var belowPriceLabel: UILabel!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var topPriceLabel: UILabel!
    @IBOutlet weak var noOfPassLabel: UILabel!
    var noOfPass: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         let attrTet = NSMutableAttributedString(string: " Complimentary Wi-Fi service  \n Complimentary beer, wine and spirits \n Complimentary breakfast and afternoon snacks \n Personalized travel assistance", attributes: nil)
         topLabel.numberOfLines = 0
         topLabel.attributedText = attrTet
        
        // Apply Gradient Color
        let gradientLayer:CAGradientLayer = CAGradientLayer()
        gradientLayer.frame.size = continueToPurchaseButton.frame.size
        gradientLayer.colors =
            [ UIColor.init(red: 30.0, green: 66.0, blue: 186.0, alpha: 0.7).cgColor, UIColor.init(red: 88.0, green: 169.0, blue: 235.0, alpha: 0.7)]
        continueToPurchaseButton.layer.addSublayer(gradientLayer)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    @IBAction func decreaseNoAction(_ sender: Any) {
        if noOfPass == 1 {
                   priceDetailsLabel.text = String(noOfPass) + " one-time pass"
               }
        else if noOfPass > 1 {
            noOfPass -= 1
            self.topPriceLabel.text = "$" + String(59 * noOfPass)
            self.belowPriceLabel.text = "$" + String(59 * noOfPass)
            self.noOfPassLabel.text = String(noOfPass)
            priceDetailsLabel.text = String(noOfPass) + " one-time passes"
        }
       
       
    }
    @IBAction func increaseNoAction(_ sender: Any) {
         if noOfPass >= 1 {
            noOfPass += 1
            self.topPriceLabel.text = "$" + String(59 * noOfPass)
            self.belowPriceLabel.text = "$" + String(59 * noOfPass)
            self.noOfPassLabel.text = String(noOfPass )
            priceDetailsLabel.text = String(noOfPass) + " one-time passes"
        }
//         else if noOfPass > 1 {
//            priceDetailsLabel.text = String(noOfPass) + " one-time pass"
//        }
       
    }
    @IBAction func cancelBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func continueBtnAction(_ sender: Any) {
    }
    
}

//
//  ViewController.swift
//  CEP
//
//  Created by Christopher Pung on 1/13/20.
//  Copyright © 2020 Mobile Apps Company. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let image = UIImage(named: "United")!
        let imageView = UIImageView(frame: CGRect(origin: .zero,
                                                  size: image.size))
        imageView.contentMode = .scaleAspectFit
        imageView.image = image
        navigationItem.titleView = imageView
    }
    
    @IBAction func navigateToProfile(_ sender: Any) {
        let storyboard = UIStoryboard(name: "MyProfile", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "ProfileScreenViewController")
        self.navigationController?.pushViewController(myVC, animated: true)
       
        
    }
    
    @IBAction func navigateToHamburgerMenu(_ sender: Any) {
        let storyboard = UIStoryboard(name: "MoreMenu", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "MoreViewController")
         self.navigationController?.pushViewController(myVC, animated: true)
        
    }
    @IBAction func navigateToAirportMaps(_ sender: Any) {
   //     let storyboard = UIStoryboard(name: "AirportMaps", bundle: nil)
   //     let myVC = storyboard.instantiateViewController(withIdentifier: "AirportMapsVC")
    //     self.navigationController?.present(myVC, animated: true)
        
    }
    @IBAction func navigateToEntertainment(_ sender: Any) {
           let storyboard = UIStoryboard(name: "EntertainmentScreenStoryboard", bundle: nil)
           let myVC = storyboard.instantiateViewController(withIdentifier: "EntertainmentScreenViewController")
            self.navigationController?.pushViewController(myVC, animated: true)
           
    }
    @IBAction func navigateToCheckIn(_ sender: Any) {
           /*let storyboard = UIStoryboard(name: "CheckIn", bundle: nil)
           let myVC = storyboard.instantiateViewController(withIdentifier: "AirportMapsVC")
            self.navigationController?.pushViewController(myVC, animated: true)
           */
    }
}

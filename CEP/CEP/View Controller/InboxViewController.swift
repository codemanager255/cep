//
//  InboxViewController.swift
//  CEP
//
//  Created by Consultant on 1/22/20.
//  Copyright © 2020 Mobile Apps Company. All rights reserved.
//

import UIKit

class InboxViewController: UIViewController {
    
    var refreshControl = UIRefreshControl()
    @IBOutlet weak var scrollview: UIScrollView!
    var time = String()
    @IBOutlet weak var timeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollview.alwaysBounceVertical = true
        scrollview.bounces  = true
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        self.scrollview.addSubview(refreshControl)
        time = getCurrentTime()
        timeLabel.text = "Last refreshed: " + time

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func getCurrentTime() -> String{
        let now = Date()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "h:mm a EEE, MMM dd, yyyy"
        let dateString = formatter.string(from: now)
        return dateString
    }
    
    @objc func didPullToRefresh(){
        let timestring = getCurrentTime()
        timeLabel.text = "Last refreshed: " + timestring
        refreshControl.endRefreshing()
    }


}

//
//  AirportMapsTableCell.swift
//  CEP
//
//  Created by Consultant on 1/31/20.
//  Copyright © 2020 Mobile Apps Company. All rights reserved.
//

import UIKit

class AirportMapsTableCell: UITableViewCell {

    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var airportLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

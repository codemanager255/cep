//
//  MoreViewController.swift
//  CEP
//
//  Created by Consultant on 1/15/20.
//  Copyright © 2020 Mobile Apps Company. All rights reserved.
//

import UIKit

class MoreViewController: UIViewController {
    
    @IBOutlet weak var tableview: UITableView!
    var optionArr = [["Flight Status", "Check-in", "Airport maps", "Check Bag Charges", "TimeTable" , "TrackMyBags", "Currency Converter", "United club and lounges", "Uber", "Wheelchair request service" , "Book a car", "Book a hotel"], ["Wifi and Entertainment", "Games"], ["Sign in to my account","MileagePlus Programs", "Enroll in MileagePlus"], ["App settings","App feedback", "Helpful app tips", "Location services FAQ"], ["Contact us", "Legal"]]
    
    let headerTitles = ["AT THE AIRPORT", "ON BOARD", "MILEAGEPLUS", "APP TOOLS", "ABOUT UNITED"]

    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.delegate = self
        tableview.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
       // let vc = MoreViewController()
        
      //  self.navigationController?.dismiss(animated: true, completion: nil)
        
       
    }
    
}

extension MoreViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return optionArr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return optionArr[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "MoreTableViewCell")
    
        if cell == nil {
            tableView.register(UINib(nibName: "MoreTableViewCell", bundle: nil), forCellReuseIdentifier: "MoreTableViewCell")
            cell = (tableView.dequeueReusableCell(withIdentifier: "MoreTableViewCell") as? MoreTableViewCell)!
        }
        let celltext = optionArr[indexPath.section][indexPath.row]

        cell?.textLabel?.text = celltext
        return cell!
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section < headerTitles.count {
               return headerTitles[section]
           }

           return nil
    }
    
    
}

//
//  ProfileScreenViewController.swift
//  CEP
//
//  Created by Consultant on 1/18/20.
//  Copyright © 2020 Mobile Apps Company. All rights reserved.
//

import UIKit

class ProfileScreenViewController: UIViewController {

    @IBOutlet weak var purchaseButton: UIButton! {
        didSet{
            let layer = purchaseButton.layer
            layer.cornerRadius = 5.0
            layer.masksToBounds = true
        }
    }
    @IBOutlet weak var signInButton: UIButton! {
        didSet{
            let layer = signInButton.layer
            layer.cornerRadius = 5.0
            layer.masksToBounds = true
            
            // Shadow and Radius
            signInButton.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
            signInButton.layer.shadowOffset = CGSize(width: 0.0, height: 8.0)
            signInButton.layer.shadowOpacity = 1.0
            signInButton.layer.shadowRadius = 10.0
            signInButton.layer.masksToBounds = false
            signInButton.layer.cornerRadius = 4.0
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       // navigationController?.setNavigationBarHidden(true, animated: animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }

    @IBAction func purchaseBtnAction(_ sender: Any) {
    }
    @IBAction func dismissAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
